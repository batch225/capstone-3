import './App.css';
import { useState, useEffect } from 'react';
import { UserProvider } from './UserContext';
import Navbar from './components/Navbar';
import Home from './pages/Home';
import Products from './pages/Products';
import ProductView from './components/ProductView';
import Register from './pages/Register';
import Login from './pages/Login';
import Logout from './pages/Logout';
import Error from './pages/Error';
import Dashboard from './pages/Dashboard'
import AdminProductView from './components/AdminProductView'
import AddProducts from './components/AddProductView';
import UpdateProducts from './components/UpdateProductView';
import Footer from './components/Footer';
import { Container } from 'react-bootstrap';
import { BrowserRouter as Router, Route, Routes } from 'react-router-dom';

function App() {
  const [user, setUser] = useState({
    id: null,
    isAdmin: null
  });

  const unsetUser = () => {
    localStorage.clear();
    setUser({
      id: null,
      isAdmin: null
    });
  };

  useEffect(() => {
    const token = localStorage.getItem('token');

    if (token) {
      fetch(`${process.env.REACT_APP_API_URL}/users/details`, {
        headers: {
          Authorization: `Bearer ${token}`
        }
      })
        .then(res => res.json())
        .then(data => {
          if (typeof data._id !== 'undefined') {
            setUser({
              id: data._id,
              isAdmin: data.isAdmin
            });
          }
        })
        .catch(error => console.error('Error fetching user details:', error));
    }
  }, []);

  return (
    <>
    <Container className='main'>
      <UserProvider value={{ user, setUser, unsetUser }}>
        <Router>
          <Container>
            <Navbar />
          </Container>
          <Container className='main-content'>
            <Routes>
              <Route path="/" element={<Home />} />
              <Route path="/products" element={<Products />} />
              <Route path="/products/:productId" element={<ProductView />} />
              <Route path="/dashboard" element={<Dashboard />} />
              <Route path="/addproducts" element={<AddProducts />} />
              <Route path="/updateproducts/:productId" element={<UpdateProducts />} />
              <Route path="/manageproducts/:productId" element={<AdminProductView />} />
              <Route path="/login" element={<Login />} />
              <Route path="/register" element={<Register />} />
              <Route path="/logout" element={<Logout />} />
              <Route path="*" element={<Error />} />
            </Routes>
          </Container>
          <Container>
            <Footer/>
          </Container>
        </Router>
      </UserProvider>
    </Container>
    </>
  );
}

export default App;


