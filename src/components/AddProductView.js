import { useState } from "react";
import { useNavigate } from "react-router-dom";
import { Card, Container, Button, Form } from 'react-bootstrap';
// import UserContext from "../UserContext";
import Swal from "sweetalert2";


export default function AddProducts () {

  const navigate = useNavigate();

  const [productName, setProductName] = useState("");
  const [description, setDescription] = useState("");
  const [price, setPrice] = useState("");

  const addItem = (data) => {
        fetch(`${process.env.REACT_APP_API_URL}/products/addProduct`, {
          method: "POST",
          headers: {
            "Content-Type": "application/json",
            Authorization: `Bearer ${localStorage.getItem("token")}`,
          },
          body: JSON.stringify(data),
        })
          .then((response) => response.json())
          .then((result) => {
            if (result) {
              Swal.fire({
                title: "Success!",
                icon: "success",
                text: "You have successfully added a product",
              });

              navigate("/dashboard");
            } else {
              console.log(result);

              Swal.fire({
                title: "Something went wrong!",
                icon: "error",
                text: "Please try again :(",
              });
            }
          });
  };
    
  const handleSubmit = (e) => {
    e.preventDefault();
    if (!productName || !description || !price) {
      Swal.fire({
        title: "All fields must be filled out!",
        icon: "error",
        text: "Please enter values for all fields.",
      });
      return;
    }
    const data = {
      productName,
      description,
      price,
    };
    addItem(data);
  };  

  return (
    <Container className="h-form d-flex align-items-center justify-content-center">
    <div className="v-form w-100">
    <Card className="form p-5">
    <h3 className="text-center p-4">Add a new Product</h3>
    <Form onSubmit={handleSubmit} border={true}>
      <Form.Group className="mb-3" controlId="productName">
        <Form.Label>Product Name</Form.Label>
        <Form.Control className="input" type="text" placeholder="Enter product name" value={productName} onChange={(e) => setProductName(e.target.value)} />
      </Form.Group>

      <Form.Group className="mb-3" controlId="description">
        <Form.Label>Description</Form.Label>
        <Form.Control className="input" type="text" placeholder="Enter description" value={description} onChange={(e) => setDescription(e.target.value)} />
      </Form.Group>

      <Form.Group className="mb-3" controlId="price">
         <Form.Label>Price</Form.Label>
         <Form.Control className="input" type="number" placeholder="Enter price" value={price} onChange={(e) => setPrice(e.target.value)} />
      </Form.Group>
      <Button variant="primary" type="submit">
        Submit
      </Button>
    </Form>
    </Card>
    </div>
    </Container>
  );
}
