import { Col, Card, CardImg } from "react-bootstrap";
import { Link } from "react-router-dom";
import ipad from "../img/ipadmini.jpg";
import iphone from '../img/iphone.jpg';
import macbook from "../img/macbook-air.jpg";
import macbookpro from "../img/macbook-pro.jpg";
import ipad10 from "../img/ipad10.jpg";
import iphone14 from "../img/iphone14.jpg";


export default function AdminProductCard(product) {
  const { productName, description, price, image, _id, isActive, createdon } =
    product.productProp;

  let img = "";
  switch (image) {
    case "ipad":
      img = ipad;
      break;
    case "iphone":
      img = iphone;
      break;
    case "macbook":
      img = macbook;
      break;
    case "macbook-pro":
      img = macbookpro;
      break;
    case "ipad10":
      img = ipad10;
      break;
    case "iphone14":
      img = iphone14;
      break;
    default:
      img = "";
  }

  let availability = "";
  switch (isActive) {
    case "true":
      availability = "In Stock";
      break;
    case "false":
      availability = "Out of Stock";
      break;
    default:
      availability = "";
  }

  return (
    <>
      <Col className="d-flex px-4 py-4" xs={12} md={6} lg={4}>
        <Card className="productCard p-5">
          <Card.Body>
            <Card.Title>{productName}</Card.Title>
            <Card.Subtitle>Description:</Card.Subtitle>
            <Card.Text>{description}</Card.Text>
            <Card.Subtitle>Price:</Card.Subtitle>
            <Card.Text>PHP {price}</Card.Text>
            <Card.Subtitle>Availability</Card.Subtitle>
            <Card.Text>{availability}</Card.Text>
            <Card.Subtitle>Created on:</Card.Subtitle>
            <Card.Text>{createdon}</Card.Text>
            <CardImg top={true.toString()} src={img} />
            <Link className="btn" to={`/manageproducts/${_id}`}>
              Manage this product
            </Link>
          </Card.Body>
        </Card>
      </Col>
    </>
  );
}
