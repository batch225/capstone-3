import { useState } from "react";
import { useParams, useNavigate } from "react-router-dom";
import { Card, Container, Button, Form } from "react-bootstrap";
import Swal from "sweetalert2";

export default function UpdateProducts() {
  const { productId } = useParams();
  const navigate = useNavigate();
  const [productName, setProductName] = useState("");
  const [description, setDescription] = useState("");
  const [price, setPrice] = useState("");

  const updateItem = (data) => {
    fetch(`${process.env.REACT_APP_API_URL}/products/${productId}`)
    .then((response) => response.json())
    .then((result) => {
      const product = {
        productId: result._id,
      };
    fetch(`${process.env.REACT_APP_API_URL}/products/${product.productId}`, {
      method: "PUT",
      headers: {
        "Content-Type": "application/json",
        Authorization: `Bearer ${localStorage.getItem("token")}`,
      },
      body: JSON.stringify(data),
    })
      .then((response) => response.json())
      .then((result) => {
        if (result) {
          Swal.fire({
            title: "Success!",
            icon: "success",
            text: "You have successfully updated the product",
          });
          navigate("/dashboard");
        } else {
          console.log(result);
          Swal.fire({
            title: "Something went wrong!",
            icon: "error",
            text: "Please try again :(",
          });
        }
        })
      });
  };

  const handleSubmit = (e) => {
    e.preventDefault();
    const data = {
      productName,
      description,
      price,
      productId,
    };
    updateItem(data);
  };

  return (
    <Container className="h-form d-flex align-items-center justify-content-center">
      <div className="v-form w-100">
        <Card className="form p-5">
          <h3 className="text-center p-4">Update a Product</h3>
          <Form onSubmit={handleSubmit}>
            <Form.Group className="mb-3" controlId="productName">
              <Form.Label>Product Name</Form.Label>
              <Form.Control
                className="input"
                type="text"
                placeholder="Enter updated product name"
                value={productName}
                onChange={(e) => setProductName(e.target.value)}
              />
            </Form.Group>

            <Form.Group className="mb-3" controlId="description">
              <Form.Label>Description</Form.Label>
              <Form.Control
                className="input"
                type="text"
                placeholder="Enter updated description"
                value={description}
                onChange={(e) => setDescription(e.target.value)}
              />
            </Form.Group>

            <Form.Group className="mb-3" controlId="price">
              <Form.Label>Price</Form.Label>
              <Form.Control
                className="input"
                type="number"
                placeholder="Enter updated price"
                value={price}
                onChange={(e) => setPrice(e.target.value)}
              />
            </Form.Group>
            <Button className="btn" variant="primary" type="submit">

              Submit
            </Button>
          </Form>
        </Card>
      </div>
    </Container>
  );
}
