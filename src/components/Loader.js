import {Spinner} from 'react-bootstrap'

export default function Loading(){
	return(
		<>
			<Spinner className="spinner" animation="border" variant="primary" />
		</>
		)
}