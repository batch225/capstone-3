import { Row, Col, Card, CardImg } from 'react-bootstrap';
import watch from '../img/watch.svg';
import homepod from '../img/homepod.jpg';
import service from '../img/service.jpg';

export default function Highlights() {
  return (
    <Row className="mt-4 mb-3">
      <Col xs={12} md={4} className="mb-3">
        <Card className="h-100 highlight">
          <Card.Body>
            <Card.Title>
              <h2>What's new?</h2>
            </Card.Title>
            <Card.Subtitle>Available on March 1st</Card.Subtitle>
            <CardImg
              className="img-fluid mt-3"
              top={true.toString()}
              src={watch}
            />
          </Card.Body>
        </Card>
      </Col>
      <Col xs={12} md={4} className="mb-3">
        <Card className="h-100 highlight">
          <Card.Body>
            <Card.Title>
              <h2>Item/s on Sale</h2>
            </Card.Title>
            <Card.Subtitle>Homepod Mini</Card.Subtitle>
            <CardImg
              className="img-fluid mt-3"
              top={true.toString()}
              src={homepod}
            />
          </Card.Body>
        </Card>
      </Col>
      <Col xs={12} md={4} className="mb-3">
        <Card className="h-100 highlight">
          <Card.Body>
            <Card.Title>
              <h2>Service and Support</h2>
            </Card.Title>
            <Card.Subtitle>We're here for you</Card.Subtitle>
            <CardImg
              className="img-fluid mt-3"
              top={true.toString()}
              src={service}
            />
          </Card.Body>
        </Card>
      </Col>
    </Row>
  );
}
