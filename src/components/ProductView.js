import { useState, useEffect, useContext } from "react";
import { Container, Card, Button, CardImg } from "react-bootstrap";
import { useParams, useNavigate, Link } from "react-router-dom";
import UserContext from "../UserContext";
import Swal from "sweetalert2";
import ipad from '../img/ipadmini.jpg';
import iphone from '../img/iphone.jpg';
import macbook from '../img/macbook-air.jpg';
import macbookpro from "../img/macbook-pro.jpg";
import ipad10 from "../img/ipad10.jpg";
import iphone14 from "../img/iphone14.jpg";

export default function ProductView(product) {
  const { productId } = useParams();

  const { user } = useContext(UserContext);

  const navigate = useNavigate();

  const [name, setName] = useState("");
  const [description, setDescription] = useState("");
  const [price, setPrice] = useState(0);
  const [image, setImage] = useState("");

  let img = '';
  switch (image) {
    case 'ipad':
      img = ipad;
      break;
    case 'iphone':
      img = iphone;
      break;
    case 'macbook':
      img = macbook;
      break;
    case "macbook-pro":
      img = macbookpro;
      break;
    case "ipad10":
      img = ipad10;
      break;
    case "iphone14":
      img = iphone14;
      break;
    default:
      img = '';
  }

  const buyNow = (productId) => {
    fetch(`${process.env.REACT_APP_API_URL}/products/${productId}`)
      .then((response) => response.json())
      .then((result) => {
        const product = {
          productId: result._id,
          productName: result.name,
          quantity: 1,
        };
        fetch(`${process.env.REACT_APP_API_URL}/orders/createOrder`, {
          method: "POST",
          headers: {
            "Content-Type": "application/json",
            Authorization: `Bearer ${localStorage.getItem("token")}`,
          },
          body: JSON.stringify({
            products: [product],
            userId: user.id,
          }),
        })
          .then((response) => response.json())
          .then((result) => {
            if (result) {
              Swal.fire({
                title: "Success!",
                icon: "success",
                text: "You have successfully placed an order",
              });

              navigate("/products");
            } else {
              console.log(result);

              Swal.fire({
                title: "Something went wrong!",
                icon: "error",
                text: "Please try again :(",
              });
            }
          });
      });
  };

  useEffect(() => {
    fetch(`${process.env.REACT_APP_API_URL}/products/${productId}`)
      .then((response) => response.json())
      .then((result) => {
        console.log(result.productName);
        console.log(result.price);
        console.log(result.image);
        setName(result.productName);
        setDescription(result.description);
        setPrice(result.price);
        setImage(result.image);
      });
  }, [productId]);

  return (

    <>
      <Container className="h-form d-flex align-items-center justify-content-center">
        <div className="v-form w-100">
          <Card className="productCard p-5">
            <Card.Body className="text-center">
              <Card.Title>{name}</Card.Title>
              <Card.Subtitle>Description:</Card.Subtitle>
              <Card.Text>{description}</Card.Text>
              <Card.Subtitle>Price:</Card.Subtitle>
              <Card.Text>PhP {price}</Card.Text>
              <CardImg top src={img} />

              {user.id !== null ? (
                <Button onClick={() => buyNow(productId)}>Buy Now</Button>
              ) : (
                <Link className="btn" to="/login">
                  Log in to Buy Now
                </Link>
              )}
            </Card.Body>
          </Card>
        </div>
      </Container>
    </>
  );
}
