
export default function Footer() { 
    return (
    <div class="footer text-center py-3">
        <h6>iDepot &copy; 2023. All rights reserved.</h6>
    </div>
    );
}
  
  