import Carousel from 'react-bootstrap/Carousel';
import MacbookImg from '../img/Macbook-Pro.svg'
import IpadImg from '../img/iPad.svg'
import IphoneImg from '../img/iPhone14.svg'

export default function ProductCarousel() {
    return (
      <Carousel>
        <Carousel.Item className="carousel-img">
          <img
            className="carousel-img d-block w-100"
            src={MacbookImg}
            alt="Macbook Pro M2"
          />
        </Carousel.Item>
        <Carousel.Item className="carousel-img">
          <img
            className="carousel-img d-block w-100"
            src={IpadImg}
            alt="iPad 10th Generation"
          />
        </Carousel.Item>
        <Carousel.Item className="carousel-img">
          <img
            className="carousel-img d-block w-100"
            src={IphoneImg}
            alt="iPhone 14 & 14 Plus"
          />
        </Carousel.Item>
      </Carousel>
    );
}


