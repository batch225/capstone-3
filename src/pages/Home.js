import Carousel from '../components/Carousel.js'
import Highlights from '../components/Highlights'
import { Container } from 'react-bootstrap'

export default function Home() {
    return (
        <>
            <Container className='home py-3'>
            <h1 className="p-4">Hello iSheep!</h1>
                <Carousel/>
                <Highlights/>
            </Container>
        </>
    )
}