import { Navigate } from 'react-router-dom'
import UserContext from '../UserContext'
import { useEffect, useContext } from 'react'

export default function Logout(){
	const {unsetUser, setUser} = useContext(UserContext)
	unsetUser()
	useEffect(() => {
		setUser({
			id: null,
			isAdmin: null
		})
	})

	return(
		<Navigate to="/login"/>
	)
}