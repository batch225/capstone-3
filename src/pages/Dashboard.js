import AdminProductCard from "../components/AdminProductCard";
import Loader from "../components/Loader";
import { useEffect, useState } from "react";
import { useNavigate } from "react-router-dom";
import { Row } from "react-bootstrap";

export default function AllProducts() {
  const [products, setProducts] = useState([]);
  const [isLoading, setIsLoading] = useState(true);

  useEffect(() => {
    fetch(`${process.env.REACT_APP_API_URL}/products/all`)
      .then((response) => response.json())
      .then((result) => {
        console.log(result);
        setProducts(result);
        setIsLoading(false);
      });
  }, []);

  const navigate = useNavigate();

  return (
    <>
      {isLoading ? (
        <Loader />
      ) : (
        <>
          <h1 className="text-center p-4">Welcome to your Dashboard!</h1>
          <div className="d-flex align-items-center justify-content-center">
          <button className="addButton" onClick={() => navigate("/addproducts")}>
            <span>
              <svg
                xmlns="http://www.w3.org/2000/svg"
                viewBox="0 0 24 24"
                width="24"
                height="24"
              >
                <path fill="none" d="M0 0h24v24H0z"></path>
                <path
                  fill="currentColor"
                  d="M11 11V5h2v6h6v2h-6v6h-2v-6H5v-2z"
                ></path>
              </svg>{" "}
              Add a New Product
            </span>
          </button>
          </div>
          <Row className="mt-3 mb-3">
            {products.map((product) => (
              <AdminProductCard
                key={product._id}
                productId={product._id}
                productProp={product}
              />
            ))}
          </Row>
        </>
      )}
    </>
  );
}
