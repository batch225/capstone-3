import { Row, Col } from 'react-bootstrap';
import { Link } from 'react-router-dom';

export default function NotFound() {
	return (
	    <Row>
	    	<Col >
	            <h1>404 Page not found</h1>
	            <p>Go back to the <Link to="/">homepage.</Link></p>
	        </Col>
	    </Row>
	)
}