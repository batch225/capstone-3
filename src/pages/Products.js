import ProductCard from '../components/ProductCard'
import Loader from '../components/Loader'
import {useEffect, useState} from 'react'
import { Row } from "react-bootstrap"

export default function Products(){
	const [products, setProducts] = useState([])
	const [isLoading, setIsLoading] = useState(true)

	useEffect((isLoading) => {

		fetch(`${process.env.REACT_APP_API_URL}/products/`)
		.then(response => response.json())
		.then(result => {
			console.log(result)
			setProducts(
				result.map(product => {
					return (
						<ProductCard key={product._id} productProp ={product}/>
					)
				})
			)
			setIsLoading(false)
		})
	}, [])



	return(
			
				(isLoading) ?
					<Loader/>
			:
				<>
				<h1 className="text-center p-4">Available Products</h1>
        		<Row className="mt-3 mb-3">
					{products}
				</Row>
				</>
	)
}