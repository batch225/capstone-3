import { Form, Button, Container } from "react-bootstrap";
import { useState, useEffect, useContext } from "react";
import { useNavigate, Navigate } from "react-router-dom";
import UserContext from "../UserContext";
import Swal from "sweetalert2";

export default function Register() {

  const { user } = useContext(UserContext);
  const navigate = useNavigate();
  const [firstName, setFirstName] = useState("");
  const [lastName, setLastName] = useState("");
  const [mobileNumber, setMobileNumber] = useState("");
  const [email, setEmail] = useState("");
  const [password1, setPassword1] = useState("");
  const [password2, setPassword2] = useState("");
  const [isActive, setIsActive] = useState(false);

  function registerUser(event) {
    event.preventDefault();

    fetch(`${process.env.REACT_APP_API_URL}/users/checkEmail`, {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify({
        email: email,
      }),
    })
      .then((response) => response.json())
      .then((result) => {
        if (result === true) {
          Swal.fire({
            title: "Oops!",
            icon: "error",
            text: "Email already exist!",
          });
        } else {
          fetch(`${process.env.REACT_APP_API_URL}/users/register`, {
            method: "POST",
            headers: {
              "Content-Type": "application/json",
            },
            body: JSON.stringify({
              firstName: firstName,
              lastName: lastName,
              mobileNumber: mobileNumber,
              email: email,
              password: password1,
            }),
          })
            .then((response) => response.json())
            .then((result) => {
              console.log(result);

              setEmail("");
              setPassword1("");
              setPassword2("");
              setFirstName("");
              setLastName("");
              setMobileNumber("");

              if (result) {
                Swal.fire({
                  title: "Register Successful!",
                  icon: "success",
                  text: "Welcome to iDepot!",
                });

                navigate("/login");
              } else {
                Swal.fire({
                  title: "Registration Failed",
                  icon: "error",
                  text: "Some of the data entered is invalid. Please check your input and try again",
                });
              }
            });
        }
      });
  }


  useEffect(() => {
    if (
      firstName !== "" &&
      lastName !== "" &&
      mobileNumber.length === 11 &&
      email !== "" &&
      password1 !== "" &&
      password2 !== "" &&
      password1 === password2
    ) {
      setIsActive(true);
    } else {
      setIsActive(false);
    }
  }, [firstName, lastName, mobileNumber, email, password1, password2]);

  return (user.id !== null) ? (
    <Navigate to="/products" />
  ) : (
    <Container className="h-form d-flex align-items-center justify-content-center">
     <div className="v-form w-100">
      <Form className="form" onSubmit={(event) => registerUser(event)}>
       <h5 className="heading">Register</h5>
        <Form.Group controlId="firstName">
          <Form.Label>First Name</Form.Label>
          <Form.Control
            className="input"
            type="text"
            placeholder="Enter First Name"
            value={firstName}
            onChange={(event) => setFirstName(event.target.value)}
            required
          />
        </Form.Group>

        <Form.Group controlId="lastName">
          <Form.Label>Last Name</Form.Label>
          <Form.Control
            className="input"
            type="text"
            placeholder="Enter Last Name"
            value={lastName}
            onChange={(event) => setLastName(event.target.value)}
            required
          />
        </Form.Group>

        <Form.Group controlId="mobileNumber">
          <Form.Label>Mobile Number</Form.Label>
          <Form.Control
            className="input"
            type="text"
            placeholder="Enter Mobile Number"
            value={mobileNumber}
            onChange={(event) => setMobileNumber(event.target.value)}
            required
          />
        </Form.Group>

        <Form.Group controlId="userEmail">
          <Form.Label>Email address</Form.Label>
          <Form.Control
            className="input"
            type="email"
            placeholder="Enter email"
            value={email}
            onChange={(event) => setEmail(event.target.value)}
            required
          />
          <Form.Text className="text-muted">
            We'll never share your email with anyone else.
          </Form.Text>
        </Form.Group>

        <Form.Group controlId="password1">
          <Form.Label>Password</Form.Label>
          <Form.Control
            className="input"
            type="password"
            placeholder="Password"
            value={password1}
            onChange={(event) => setPassword1(event.target.value)}
            required
          />
        </Form.Group>

        <Form.Group controlId="password2">
          <Form.Label>Verify Password</Form.Label>
          <Form.Control
            className="input"
            type="password"
            placeholder="Verify Password"
            value={password2}
            onChange={(event) => setPassword2(event.target.value)}
            required
          />
        </Form.Group>

        {isActive ? (
          <Button className="btn" type="submit" id="submitBtn">
            Submit
          </Button>
        ) : (
          <Button type="submit" id="submitBtn" disabled>
            Submit
          </Button>
        )}
      </Form>
      </div>
    </Container>
  );
}
