import { useState, useEffect, useContext } from "react";
import { Form, Button, Container } from "react-bootstrap";
import { Navigate } from "react-router-dom";
import UserContext from "../UserContext";
import Swal from "sweetalert2";

export default function Login() {

  const { user, setUser } = useContext(UserContext);
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const [isActive, setIsActive] = useState(false);

  const retrieveUser = (token) => {
    fetch(`${process.env.REACT_APP_API_URL}/users/details`, {
      headers: {
        Authorization: `Bearer ${token}`
      }
    })
      .then((response) => response.json())
      .then((result) => {
        console.log(result)
        setUser({
          id: result._id,
          isAdmin: result.isAdmin
        })
      })
  }

  function authenticate(event) {
    event.preventDefault();

    fetch(`${process.env.REACT_APP_API_URL}/users/login`, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({
        email: email,
        password: password
      })
    })
    .then(response => response.json())
    .then(result => {
        if(typeof result.access !== "undefined"){
          localStorage.setItem("token", result.access);

          retrieveUser(result.access);

          Swal.fire({
            title: "Login Successful!",
            icon: "success",
            text: "Welcome to iDepot!"
          })
        } else {
          Swal.fire({
            title: "Authentication Failed!",
            icon: "error",
            text: "Invalid Email or password"
          })
        }
      })
  }

  useEffect(() => {
    if ((email !== '' && password !== '')){
      setIsActive(true);
    } else {
      setIsActive(false);
    }
  }, [email, password])

  return(
    (user.id !== null && user.isAdmin === true) ?
    <Navigate to="/dashboard"/> 
    :
    (user.id !== null && user.isAdmin === false) ?
    <Navigate to="/products"/>
    :
    <Container
      className="h-form d-flex align-items-center justify-content-center"
    >
      <div className="v-form w-100">
        <Form className="form" onSubmit={(event) => authenticate(event)}>
         <h5 className="heading">Login</h5>
          <Form.Group controlId="userEmail">
            <Form.Label>Email Address</Form.Label>
            <Form.Control
              className="input"
              type="email"
              placeholder="Enter email"
              value={email}
              onChange={(event) => setEmail(event.target.value)}
              required
            />
            <Form.Text className="text-muted">
              We'll never share your email with anyone else.
            </Form.Text>
          </Form.Group>

          <Form.Group controlId="password">
            <Form.Label>Password</Form.Label>
            <Form.Control
              className="input"
              type="password"
              placeholder="Password"
              value={password}
              onChange={(event) => setPassword(event.target.value)}
              required
            />
          </Form.Group>

          {isActive ? (
            <Button className="btn" type="submit" id="submitBtn">
              Submit
            </Button>
          ) : (
            <Button type="submit" id="submitBtn" disabled>
              Submit
            </Button>
          )}
        </Form>
      </div>
    </Container>
  );
}